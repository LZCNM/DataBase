一 创建Redis集群
	1.1 集群的功能： 可以实现数据的自动备份
		           可以实现redis服务的高可用
		           分布式存储数据


	1.2  启用每一台redis服务的集群功能 (15分钟14:41)
已51主机为例
[root@mysql51 ~]# /etc/init.d/redis_6379  stop
Stopping ...
Redis stopped
[root@mysql51 ~]# vim   /etc/redis/6379.conf 

port 6351
bind 192.168.4.51
cluster-enabled yes
cluster-config-file nodes-6379.conf
cluster-node-timeout 5000
:wq

[root@mysql51 ~]# netstat  -utnlp  | grep  redis-server
tcp        0      0 192.168.4.51:16351      0.0.0.0:*               LISTEN      13775/redis-server  
tcp        0      0 192.168.4.51:6351       0.0.0.0:*               LISTEN      13775/redis-server  
[root@mysql51 ~]#

[root@mysql51 ~]# redis-cli  -h 192.168.4.51 -p 6351       //51 主机自己访问自己
192.168.4.51:6351> cluster   info  查看集群信息
cluster_state:fail
cluster_slots_assigned:0
cluster_slots_ok:0
cluster_slots_pfail:0
cluster_slots_fail:0
cluster_known_nodes:1
cluster_size:0
cluster_current_epoch:0
cluster_my_epoch:0
cluster_stats_messages_sent:0
cluster_stats_messages_received:0
192.168.4.51:6351> 
192.168.4.51:6351> cluster   nodes  查看集群成员列表
520f49d221e0636e8e25f747bb71fa3ea8d3b469 :6351@16351 myself,master - 0 0 0 connected
192.168.4.51:6351> 
192.168.4.51:6351> exit

[root@mysql51 ~]# ls /var/lib/redis/6379/   
dump.rdb  nodes-6379.conf
[root@mysql51 ~]# 
[root@mysql51 ~]# cat /var/lib/redis/6379/nodes-6379.conf   查看保存集群信息文件
520f49d221e0636e8e25f747bb71fa3ea8d3b469 :0@0 myself,master - 0 0 0 connected
vars currentEpoch 0 lastVoteEpoch 0
[root@mysql51 ~]#  

	1.2 配置管理主机 57 
		1.2.1 准备脚本运行环境     5分钟时间到 15:30
			]# yum  -y  install  ruby  rubygems 
			]# gem  install  redis-3.2.1.gem

		1.2.2  创建管理集群的ruby 脚本
			[root@HOST57 ~]# cd redis-4.0.8/
[root@HOST57 redis-4.0.8]# ls
00-RELEASENOTES  COPYING  Makefile   redis.conf       runtest-sentinel  tests
BUGS             deps     MANIFESTO  runtest          sentinel.conf     utils
CONTRIBUTING     INSTALL  README.md  runtest-cluster  src
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# ls src/*.rb  
src/redis-trib.rb
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# echo  $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# cd /root/bin
-bash: cd: /root/bin: No such file or directory
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# mkdir /root/bin
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# cp src/redis-trib.rb /root/bin/  
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# chmod +x /root/bin/redis-trib.rb  
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# ls -l /root/bin/redis-trib.rb 
-rwxr-xr-x. 1 root root 65991 Jun  1 15:13 /root/bin/redis-trib.rb
[root@HOST57 redis-4.0.8]# 
[root@HOST57 redis-4.0.8]# redis-trib.rb help  查看命令帮助信息
Usage: redis-trib <command> <options> <arguments ...>

  create          host1:port1 ... hostN:portN
                  --replicas <arg>
  check           host:port
  info            host:port
  fix             host:port
                  --timeout <arg>
  reshard         host:port
                  --from <arg>
                  --to <arg>
                  --slots <arg>
                  --yes
                  --timeout <arg>
                  --pipeline <arg>
  rebalance       host:port
                  --weight <arg>
                  --auto-weights
                  --use-empty-masters
                  --timeout <arg>
                  --simulate
                  --pipeline <arg>
                  --threshold <arg>
  add-node        new_host:new_port existing_host:existing_port
                  --slave
                  --master-id <arg>
  del-node        host:port node_id
  set-timeout     host:port milliseconds
  call            host:port command arg arg .. arg
  import          host:port
                  --from <arg>
                  --copy
                  --replace
  help            (show this help)

For check, fix, reshard, del-node, set-timeout you can specify the host and port of any working node in the cluster.
[root@HOST57 redis-4.0.8]# 

		1.3  创建集群
[root@HOST57 ~]# redis-trib.rb  create --replicas 1  192.168.4.51:6351  192.168.4.52:6352 192.168.4.53:6353 192.168.4.54:6354 192.168.4.55:6355 192.168.4.56:6356			
>>> Creating cluster
>>> Performing hash slots allocation on 6 nodes...
Using 3 masters:
192.168.4.51:6351
192.168.4.52:6352
192.168.4.53:6353
Adding replica 192.168.4.55:6355 to 192.168.4.51:6351
Adding replica 192.168.4.56:6356 to 192.168.4.52:6352
Adding replica 192.168.4.54:6354 to 192.168.4.53:6353
M: 520f49d221e0636e8e25f747bb71fa3ea8d3b469 192.168.4.51:6351
   slots:0-5460 (5461 slots) master
M: c4e1bcef71c4a9b799b29a9c666836962fc81eda 192.168.4.52:6352
   slots:5461-10922 (5462 slots) master
M: 8f6c32e447d978c9750ff81cc1b13fee02bac875 192.168.4.53:6353
   slots:10923-16383 (5461 slots) master
S: e5e96b0e27c8007a274de6d4a28f8ad143c502d2 192.168.4.54:6354
   replicates 8f6c32e447d978c9750ff81cc1b13fee02bac875
S: 2e79e70442de0426d35c6e8d1ab145373a11a63f 192.168.4.55:6355
   replicates 520f49d221e0636e8e25f747bb71fa3ea8d3b469
S: 8cbaacec314fcc4028811fb6c0ae008b6f87f2f2 192.168.4.56:6356
   replicates c4e1bcef71c4a9b799b29a9c666836962fc81eda
Can I set the above configuration? (type 'yes' to accept): yes	  同意
>>> Nodes configuration updated
>>> Assign a different config epoch to each node
>>> Sending CLUSTER MEET messages to join the cluster
Waiting for the cluster to join..
>>> Performing Cluster Check (using node 192.168.4.51:6351)
M: 520f49d221e0636e8e25f747bb71fa3ea8d3b469 192.168.4.51:6351
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
S: e5e96b0e27c8007a274de6d4a28f8ad143c502d2 192.168.4.54:6354
   slots: (0 slots) slave
   replicates 8f6c32e447d978c9750ff81cc1b13fee02bac875
M: 8f6c32e447d978c9750ff81cc1b13fee02bac875 192.168.4.53:6353
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
S: 2e79e70442de0426d35c6e8d1ab145373a11a63f 192.168.4.55:6355
   slots: (0 slots) slave
   replicates 520f49d221e0636e8e25f747bb71fa3ea8d3b469
M: c4e1bcef71c4a9b799b29a9c666836962fc81eda 192.168.4.52:6352
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
S: 8cbaacec314fcc4028811fb6c0ae008b6f87f2f2 192.168.4.56:6356
   slots: (0 slots) slave
   replicates c4e1bcef71c4a9b799b29a9c666836962fc81eda
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
[root@HOST57 ~]# 

[root@mysql51 ~]# redis-cli  -h 192.168.4.51 -p 6351     //51主机自己访问自己
192.168.4.51:6351> cluster info  查看集群信息
cluster_state:ok   状态
cluster_slots_assigned:16384
cluster_slots_ok:16384
cluster_slots_pfail:0
cluster_slots_fail:0
cluster_known_nodes:6  主机台数
cluster_size:3  主服务器个数
cluster_current_epoch:6
cluster_my_epoch:1
cluster_stats_messages_ping_sent:119
cluster_stats_messages_pong_sent:116
cluster_stats_messages_sent:235
cluster_stats_messages_ping_received:111
cluster_stats_messages_pong_received:119
cluster_stats_messages_meet_received:5
cluster_stats_messages_received:235
192.168.4.51:6351> 
192.168.4.51:6351> cluster  nodes   集群成员列表
e5e96b0e27c8007a274de6d4a28f8ad143c502d2 192.168.4.54:6354@16354 slave 8f6c32e447d978c9750ff81cc1b13fee02bac875 0 1590997365092 4 connected
520f49d221e0636e8e25f747bb71fa3ea8d3b469 192.168.4.51:6351@16351 myself,master - 0 1590997365000 1 connected 0-5460
8f6c32e447d978c9750ff81cc1b13fee02bac875 192.168.4.53:6353@16353 master - 0 1590997365000 3 connected 10923-16383
2e79e70442de0426d35c6e8d1ab145373a11a63f 192.168.4.55:6355@16355 slave 520f49d221e0636e8e25f747bb71fa3ea8d3b469 0 1590997365000 5 connected
c4e1bcef71c4a9b799b29a9c666836962fc81eda 192.168.4.52:6352@16352 master - 0 1590997366301 2 connected 5461-10922
8cbaacec314fcc4028811fb6c0ae008b6f87f2f2 192.168.4.56:6356@16356 slave c4e1bcef71c4a9b799b29a9c666836962fc81eda 0 1590997365295 6 connected
192.168.4.51:6351> 
192.168.4.51:6351> exit
[root@mysql51 ~]# 
[root@mysql51 ~]# 
[root@mysql51 ~]# ls /var/lib/redis/6379/
dump.rdb  nodes-6379.conf
[root@mysql51 ~]# 
[root@mysql51 ~]# cat /var/lib/redis/6379/nodes-6379.conf   查看集群信息文件的内容
e5e96b0e27c8007a274de6d4a28f8ad143c502d2 192.168.4.54:6354@16354 slave 8f6c32e447d978c9750ff81cc1b13fee02bac875 0 1590997278000 4 connected
520f49d221e0636e8e25f747bb71fa3ea8d3b469 192.168.4.51:6351@16351 myself,master - 0 1590997276000 1 connected 0-5460
8f6c32e447d978c9750ff81cc1b13fee02bac875 192.168.4.53:6353@16353 master - 0 1590997279000 3 connected 10923-16383
2e79e70442de0426d35c6e8d1ab145373a11a63f 192.168.4.55:6355@16355 slave 520f49d221e0636e8e25f747bb71fa3ea8d3b469 0 1590997277629 5 connected
c4e1bcef71c4a9b799b29a9c666836962fc81eda 192.168.4.52:6352@16352 master - 0 1590997278000 2 connected 5461-10922
8cbaacec314fcc4028811fb6c0ae008b6f87f2f2 192.168.4.56:6356@16356 slave c4e1bcef71c4a9b799b29a9c666836962fc81eda 0 1590997279701 6 connected
vars currentEpoch 6 lastVoteEpoch 0
[root@mysql51 ~]# 
	
 
二 管理Redis集群       练习 + 休息到  17:15 
	查看集群信息
[root@HOST57 ~]# redis-trib.rb info  192.168.4.53:6353
192.168.4.53:6353 (8f6c32e4...) -> 0 keys | 5461 slots | 1 slaves.
192.168.4.51:6351 (520f49d2...) -> 0 keys | 5461 slots | 1 slaves.
192.168.4.52:6352 (c4e1bcef...) -> 0 keys | 5462 slots | 1 slaves.
[OK] 0 keys in 3 masters.
0.00 keys per slot on average.
[root@HOST57 ~]# 


	查看详细信息
[root@HOST57 ~]# redis-trib.rb  check  192.168.4.53:6353
>>> Performing Cluster Check (using node 192.168.4.53:6353)
M: 8f6c32e447d978c9750ff81cc1b13fee02bac875 192.168.4.53:6353
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
S: e5e96b0e27c8007a274de6d4a28f8ad143c502d2 192.168.4.54:6354
   slots: (0 slots) slave
   replicates 8f6c32e447d978c9750ff81cc1b13fee02bac875
M: 520f49d221e0636e8e25f747bb71fa3ea8d3b469 192.168.4.51:6351
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
S: 8cbaacec314fcc4028811fb6c0ae008b6f87f2f2 192.168.4.56:6356
   slots: (0 slots) slave
   replicates c4e1bcef71c4a9b799b29a9c666836962fc81eda
M: c4e1bcef71c4a9b799b29a9c666836962fc81eda 192.168.4.52:6352
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
S: 2e79e70442de0426d35c6e8d1ab145373a11a63f 192.168.4.55:6355
   slots: (0 slots) slave
   replicates 520f49d221e0636e8e25f747bb71fa3ea8d3b469
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
[root@HOST57 ~]# 


	客户端访问集群存/取数据

[root@mysql50 ~]# redis-cli -c -h 192.168.4.52  -p 6352
192.168.4.52:6352> keys *
(empty list or set)
192.168.4.52:6352> 
192.168.4.52:6352> set name yaya
OK
192.168.4.52:6352> set age girl
-> Redirected to slot [741] located at 192.168.4.51:6351
OK
192.168.4.51:6351> keys *
1) "age"
192.168.4.51:6351> set school  tarena
-> Redirected to slot [8455] located at 192.168.4.52:6352
OK
192.168.4.52:6352> set x 99
-> Redirected to slot [16287] located at 192.168.4.53:6353
OK
192.168.4.53:6353> keys *
1) "x"
192.168.4.53:6353> get x
"99"
192.168.4.53:6353> get age
-> Redirected to slot [741] located at 192.168.4.51:6351
"girl"
192.168.4.51:6351> keys *
1) "age"
192.168.4.51:6351> get name
-> Redirected to slot [5798] located at 192.168.4.52:6352
"yaya"
192.168.4.52:6352> exit




	测试集群的高可用功能和数据自动备份功能
		数据自动备份功能
[root@mysql55 ~]# redis-cli -c -h 192.168.4.56  -p 6356
192.168.4.56:6356> keys *
1) "name"
2) "school"
192.168.4.56:6356> exit
[root@mysql55 ~]# redis-cli -c -h 192.168.4.55  -p 6355
192.168.4.55:6355> keys *
1) "age"
192.168.4.55:6355> exit
[root@mysql55 ~]# 
[root@mysql55 ~]# redis-cli -c -h 192.168.4.54  -p 6354
192.168.4.54:6354> keys *
1) "x"
192.168.4.54:6354> get x
-> Redirected to slot [16287] located at 192.168.4.53:6353
"99"
192.168.4.53:6353> 
			测试集群的高可用功能
[root@HOST57 ~]# redis-trib.rb  info  192.168.4.51:6351
192.168.4.51:6351 (520f49d2...) -> 1 keys | 5461 slots | 1 slaves.
192.168.4.53:6353 (8f6c32e4...) -> 1 keys | 5461 slots | 1 slaves.
192.168.4.52:6352 (c4e1bcef...) -> 2 keys | 5462 slots | 1 slaves.
[OK] 4 keys in 3 masters.
0.00 keys per slot on average.
[root@HOST57 ~]#

[root@mysql51 ~]# redis-cli  -h 192.168.4.51 -p 6351 shutdown
[root@mysql51 ~]# 
[root@mysql51 ~]# netstat  -utnlp  | grep redis-server
[root@mysql51 ~]# 

[root@HOST57 ~]# redis-trib.rb  info  192.168.4.51:6351
[ERR] Sorry, can't connect to node 192.168.4.51:6351
[root@HOST57 ~]# 
[root@HOST57 ~]# redis-trib.rb  info  192.168.4.52:6352
192.168.4.52:6352 (c4e1bcef...) -> 2 keys | 5462 slots | 1 slaves.
192.168.4.53:6353 (8f6c32e4...) -> 1 keys | 5461 slots | 1 slaves.
192.168.4.55:6355 (2e79e704...) -> 1 keys | 5461 slots | 0 slaves.
[OK] 4 keys in 3 masters.
0.00 keys per slot on average.
[root@HOST57 ~]# 

	集群存/取数据的工作过程?
					09:00  上课 
	2.1  向集群中添加新服务器  
		2.1.1 添加master角色服务器    时间 10分钟到  09:41 
                                               192.168.4.58 （安装源码redis软件 做初始化配置 并启用集群功能运行服务）
			在管理主机57，添加master角色的Redis服务器
				1 添加主机
[root@HOST57 ~]# redis-trib.rb  info  192.168.4.53:6353
192.168.4.53:6353 (8f6c32e4...) -> 2 keys | 5461 slots | 1 slaves.
192.168.4.52:6352 (c4e1bcef...) -> 3 keys | 5462 slots | 1 slaves.
192.168.4.55:6355 (2e79e704...) -> 2 keys | 5461 slots | 1 slaves.
[OK] 7 keys in 3 masters.
0.00 keys per slot on average.
[root@HOST57 ~]#

[root@HOST57 ~]# redis-trib.rb  add-node  192.168.4.58:6379   192.168.4.53:6353
>>> Adding node 192.168.4.58:6379 to cluster 192.168.4.53:6353
>>> Performing Cluster Check (using node 192.168.4.53:6353)
M: 8f6c32e447d978c9750ff81cc1b13fee02bac875 192.168.4.53:6353
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
S: e5e96b0e27c8007a274de6d4a28f8ad143c502d2 192.168.4.54:6354
   slots: (0 slots) slave
   replicates 8f6c32e447d978c9750ff81cc1b13fee02bac875
S: 520f49d221e0636e8e25f747bb71fa3ea8d3b469 192.168.4.51:6351
   slots: (0 slots) slave
   replicates 2e79e70442de0426d35c6e8d1ab145373a11a63f
S: 8cbaacec314fcc4028811fb6c0ae008b6f87f2f2 192.168.4.56:6356
   slots: (0 slots) slave
   replicates c4e1bcef71c4a9b799b29a9c666836962fc81eda
M: c4e1bcef71c4a9b799b29a9c666836962fc81eda 192.168.4.52:6352
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
M: 2e79e70442de0426d35c6e8d1ab145373a11a63f 192.168.4.55:6355
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
>>> Send CLUSTER MEET to node 192.168.4.58:6379 to make it join the cluster.
[OK] New node added correctly.
[root@HOST57 ~]#



				2 分配hash slots
[root@HOST57 ~]#  redis-trib.rb reshard 192.168.4.53:6353
移出hash槽个数   4096
接收hash槽主机ID    58主机的id
移出hash槽主机ID    all
同意配置   yes


				3 查看集群信息
[root@HOST57 ~]# redis-trib.rb info 192.168.4.53:6353
192.168.4.53:6353 (8f6c32e4...) -> 2 keys | 4096 slots | 1 slaves.
192.168.4.58:6379 (2d1ecc69...) -> 2 keys | 4096 slots | 0 slaves.
192.168.4.52:6352 (c4e1bcef...) -> 2 keys | 4096 slots | 1 slaves.
192.168.4.55:6355 (2e79e704...) -> 1 keys | 4096 slots | 1 slaves.
[OK] 7 keys in 4 masters.
0.00 keys per slot on average.
[root@HOST57 ~]#

[root@HOST57 ~]# redis-trib.rb check 192.168.4.53:6353
M: 2d1ecc690c0d841887693b3be09a41332918f55b 192.168.4.58:6379
   slots:0-1364,5461-6826,10923-12287 (4096 slots) master
   0 additional replica(s)

				4 客户端访问新添加服务器存/取数据
[root@mysql50 ~]# redis-cli  -c  -h 192.168.4.58 -p 6379
192.168.4.58:6379> keys *
1) "name"
2) "age"
192.168.4.58:6379> set address  bj
-> Redirected to slot [3680] located at 192.168.4.55:6355
OK
192.168.4.55:6355> keys *
1) "address"
2) "j"
192.168.4.55:6355> 
			

		

	2.2  把服务器从集群中移除
		2.2.1 移除slave角色服务器     练习到 11:11 
[root@HOST57 ~]# redis-trib.rb  info  192.168.4.53:6353
192.168.4.53:6353 (8f6c32e4...) -> 2 keys | 4096 slots | 1 slaves.
192.168.4.58:6379 (2d1ecc69...) -> 2 keys | 4096 slots | 0 slaves.
192.168.4.52:6352 (c4e1bcef...) -> 2 keys | 4096 slots | 1 slaves.
192.168.4.55:6355 (2e79e704...) -> 2 keys | 4096 slots | 1 slaves.
[OK] 8 keys in 4 masters.
0.00 keys per slot on average.
[root@HOST57 ~]# 


[root@HOST57 ~]# redis-trib.rb del-node 192.168.4.53:6353 e5e96b0e27c8007a274de6d4a28f8ad143c502d2

>>> Removing node e5e96b0e27c8007a274de6d4a28f8ad143c502d2 from cluster 192.168.4.53:6353
>>> Sending CLUSTER FORGET messages to the cluster...
>>> SHUTDOWN the node.
[root@HOST57 ~]# 
[root@HOST57 ~]# redis-trib.rb info  192.168.4.56:6356
192.168.4.53:6353 (8f6c32e4...) -> 2 keys | 4096 slots | 0 slaves.
192.168.4.58:6379 (2d1ecc69...) -> 2 keys | 4096 slots | 0 slaves.
192.168.4.52:6352 (c4e1bcef...) -> 2 keys | 4096 slots | 1 slaves.
192.168.4.55:6355 (2e79e704...) -> 2 keys | 4096 slots | 1 slaves.
[OK] 8 keys in 4 masters.
0.00 keys per slot on average.
[root@HOST57 ~]# 

[root@mysql54 ~]# netstat  -utnlp  | grep redis-server
[root@mysql54 ~]# 
   			
		2.2.2 移除master角色服务器
			释放占用的hash slots
]# redis-trib.rb reshard 192.168.4.56:6356
How many slots do you want to move (from 1 to 16384)? 4096   移除槽个数
What is the receiving node ID? 4361720c3978aa02347076218580a103c60a6d7f   接收槽主机id 
Please enter all the source node IDs.
  Type 'all' to use all the nodes as source nodes for the hash slots.
  Type 'done' once you entered all the source nodes IDs.
Source node #1:e081313ec843655d9bc5a17f3bed3de1dccb1d2b  移除槽主机的id 
Source node #2:done  结束主机id 的填写
Do you want to proceed with the proposed reshard plan (yes/no)? yes  同意以上配置


			删除主机
[root@HOST57 ~]# redis-trib.rb  del-node  192.168.4.54:6354 2d1ecc690c0d841887693b3be09a41332918f55b

 
	2.3 把移除的服务器再添加到集群里   
[root@mysql54 ~]# rm -rf  /var/lib/redis/6379/*
[root@mysql54 ~]# /etc/init.d/redis_6379  start
Starting Redis server...
[root@mysql54 ~]# 
[root@mysql54 ~]# redis-cli  -h 192.168.4.54 -p 6354
192.168.4.54:6354> cluster info
cluster_state:fail
cluster_slots_assigned:2
cluster_slots_ok:2
cluster_slots_pfail:0
cluster_slots_fail:0
cluster_known_nodes:1
cluster_size:1
cluster_current_epoch:0
cluster_my_epoch:0
cluster_stats_messages_sent:0
cluster_stats_messages_received:0
192.168.4.54:6354> exit
[root@mysql54 ~]# 

		2.1.2 添加slave角色服务器
[root@HOST57 ~]# redis-trib.rb add-node --slave 192.168.4.54:6354 192.168.4.58:6379
>>> Adding node 192.168.4.54:6354 to cluster 192.168.4.58:6379
>>> Performing Cluster Check (using node 192.168.4.58:6379)
M: 2d1ecc690c0d841887693b3be09a41332918f55b 192.168.4.58:6379
   slots:0-1364,5461-6826,10923-12287 (4096 slots) master
   0 additional replica(s)
M: 2e79e70442de0426d35c6e8d1ab145373a11a63f 192.168.4.55:6355
   slots:1365-5460 (4096 slots) master
   0 additional replica(s)
S: 8cbaacec314fcc4028811fb6c0ae008b6f87f2f2 192.168.4.56:6356
   slots: (0 slots) slave
   replicates c4e1bcef71c4a9b799b29a9c666836962fc81eda
M: 8f6c32e447d978c9750ff81cc1b13fee02bac875 192.168.4.53:6353
   slots:12288-16383 (4096 slots) master
   0 additional replica(s)
M: c4e1bcef71c4a9b799b29a9c666836962fc81eda 192.168.4.52:6352
   slots:6827-10922 (4096 slots) master
   1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
Automatically selected master 192.168.4.58:6379
>>> Send CLUSTER MEET to node 192.168.4.54:6354 to make it join the cluster.
Waiting for the cluster to join.
>>> Configure node as replica of 192.168.4.58:6379.
[OK] New node added correctly.
[root@HOST57 ~]#	
	
[root@HOST57 ~]# redis-trib.rb  info  192.168.4.53:6353
192.168.4.53:6353 (8f6c32e4...) -> 2 keys | 4096 slots | 0 slaves.
192.168.4.58:6379 (2d1ecc69...) -> 2 keys | 4096 slots | 1 slaves.
192.168.4.52:6352 (c4e1bcef...) -> 2 keys | 4096 slots | 1 slaves.
192.168.4.55:6355 (2e79e704...) -> 2 keys | 4096 slots | 0 slaves.
[OK] 8 keys in 4 masters.
0.00 keys per slot on average.
[root@HOST57 ~]# 


[root@HOST57 ~]# redis-trib.rb  rebalance  192.168.4.54:6354  平均分配所有主服务器占用槽


		管理集群掌握的技术：
			添加master角色主机
			添加slave角色主机
			删除master主机
			删除slave主机
			把删除的主机再添加到集群 rm -rf /var/lib/redis/6379/*

			把 数据库服务器恢复为独立的服务器。
			14:38 

		
	






